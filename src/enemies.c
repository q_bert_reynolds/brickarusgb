#include "brickarus.h"
#include "music.c"

UBYTE enemy_sprite_data[] = {
    0xFF,0xFF,0x80,0xFF,0x80,0xFF,0x80,0xFF,
    0x80,0xFF,0x80,0xFF,0x80,0xFF,0x80,0xFF,
    0x80,0xFF,0x80,0xFF,0x98,0xFF,0x98,0xFF,
    0x98,0xFF,0x98,0xF7,0x80,0xFF,0xFF,0xFF,
    0x07,0x07,0x18,0x1F,0x20,0x3F,0x40,0x7F,
    0x40,0x7F,0x80,0xFF,0x80,0xFF,0x80,0xFF,
    0x80,0xFF,0x80,0xFF,0x8C,0xFF,0x4C,0x7F,
    0x48,0x7B,0x20,0x3F,0x18,0x1F,0x07,0x07
};

UBYTE sine_wave[] = {
    0,0,0,0,0,0,
    1,1,1,1,1,
    2,2,2,2,2,2,2,
    3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,
    2,2,2,2,2,2,2,
    1,1,1,1,1,1,
    0,0,0,0,0,0,0,0,0,0,
    -1,-1,-1,-1,-1,-1,
    -2,-2,-2,-2,-2,-2,
    -3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,
    -2,-2,-2,-2,-2,-2,
    -1,-1,-1,-1,-1,-1,
    0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,
    2,2,2,2,2,2,2,
    3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,
    2,2,2,2,2,2,2,
    1,1,1,1,1,
    0,0,0,0,0,0,0,0,0,0,0,
    -1,-1,-1,-1,-1,
    -2,-2,-2,-2,-2,-2,-2,
    -3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,
    -2,-2,-2,-2,-2,-2,-2,
    -1,-1,-1,-1,-1
};

struct enemy enemies[ENEMY_COUNT];

void drop_enemies () {
    for (i = 0; i < ENEMY_COLS; i++) {
        for (j = 0; j < ENEMY_ROWS; j++) {
            key = j * ENEMY_COLS + i;
            enemies[key].x_pos = i * 24 + 32;
            enemies[key].y_pos = j * 24 + 24;
            enemies[key].alive = 1;
            enemies[key].type_index = 0;

            key = key*2 + ENEMY_SPRITE_ID;
            set_sprite_tile(key, ENEMY_TILES_INDEX+enemies[i].type_index*2);
            set_sprite_prop(key, enemies[i].type_index+1);
            set_sprite_tile(key+1, ENEMY_TILES_INDEX+enemies[i].type_index*2+1);
            set_sprite_prop(key+1, S_FLIPX | (enemies[i].type_index+1));
        }
    }

    for (i = 1; i < ENEMY_COUNT; i++) {
        enemies[i].offset = enemies[i-1].offset * 107u - 7u;
    }

    for (j = 300; j > 0; j--) {
        for (i = 0; i < ENEMY_COUNT; i++) {
            key = i*2 + ENEMY_SPRITE_ID;
            y = -j + enemies[i].y_pos + sine_wave[frame+enemies[i].offset];
            move_sprite(key, enemies[i].x_pos, y);
            move_sprite(key+1, enemies[i].x_pos+8, y);
        }
        update_music();
        wait_vbl_done();
    }
}

void update_enemies () {
    temp = ENEMY_COUNT;
    for (i = 0; i < ENEMY_COUNT; i++) {
        key = i*2 + ENEMY_SPRITE_ID;
        y = enemies[i].y_pos + sine_wave[frame+enemies[i].offset];
        move_sprite(key, enemies[i].x_pos, y);
        move_sprite(key+1, enemies[i].x_pos+8, y);

        if (enemies[i].alive == 0) temp--;
    }

    if (temp == 0) drop_enemies();
}