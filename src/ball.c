#include "brickarus.h"

struct rigidbody {
    UBYTE x_pos;
    UBYTE y_pos;
    UBYTE last_x_pos;
    UBYTE last_y_pos;
    WORD x;
    WORD y;
    WORD x_vel;
    WORD y_vel;
};

struct rigidbody ball_body;

UBYTE ball_sprite_data[] = {
    0x3C,0x3C,0x7E,0x42,0xBF,0xC1,0x9F,0xE1,
    0x8F,0xF1,0x81,0xFF,0x42,0x7E,0x3C,0x3C,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

void update_ball () {
    if (ball_on_paddle) {
        ball_body.x_pos = paddle_pos+16;
        ball_body.y_pos = 128;

        key = joypad();
        if (key & J_A) {
            ball_on_paddle = 0;
            ball_body.y_vel = LAUNCH_SPEED;
            ball_body.x_vel = paddle_vel * 64;
            ball_body.x = (WORD)ball_body.x_pos * 256;
            ball_body.y = (WORD)ball_body.y_pos * 256;
        }
    }
    else {
        x = (ball_body.x / 256) - paddle_pos - 12;
        // hit ceiling
        if (ball_body.y_pos < 16 && ball_body.y_vel > 0) {
            ball_body.y_vel = -2 * ball_body.y_vel / 2; // I shouldn't have to do this.
            bounce_sound();
        }
        // lose ball
        else if (ball_body.y_pos > 154) {
            lives--;
            update_hearts();
            lose_life_sound();
            ball_on_paddle = 1;
        }
        // hit paddle
        else if (ball_body.y_pos > 130 && ball_body.y_pos < 146 && ball_body.y_vel < 0 && x > -16 && x < 24) {
            ball_body.x_vel += paddle_vel * 64 + (x-4) * 16;
            ball_body.y_vel = LAUNCH_SPEED;
            hit_paddle_sound();
            set_paddle_hit(x);
        }
        // hit wall
        else if ((ball_body.x_pos < 10 && ball_body.x_vel < 0) || (ball_body.x_pos > 166 && ball_body.x_vel > 0)) {
            ball_body.x_vel = -ball_body.x_vel / 2;
            bounce_sound();
        }
        // hit bricks
        else {
            x = (ball_body.x_pos - 30) / 24;
            y = (ball_body.y_pos - 24) / 24;
            key = y * ENEMY_COLS + x;
            if (key < ENEMY_COUNT && enemies[key].alive) {
                x = ball_body.x_pos + 4 - enemies[key].x_pos;
                y = ball_body.y_pos - 4 - enemies[key].y_pos;

                if (x < 24 && x > -24 && y < 24 && y > -24) {
                    if (x > 20 || x < -20) ball_body.x_vel = -ball_body.x_vel / 2;
                    else if (y > -24 && y < 0) ball_body.y_vel = -ball_body.y_vel / 2;
                    else ball_body.y_vel = -2 * ball_body.y_vel / 2;

                    enemies[key].alive = 0;
                    enemies[key].x_pos = 200;
                    enemies[key].y_pos = 200;
                    score += 10u;
                    hit_brick_sound();
                    update_score();
                }
            }
        }

        ball_body.x += ball_body.x_vel;
        ball_body.y -= ball_body.y_vel;
        ball_body.y_vel -= GRAVITY;
        ball_body.last_x_pos = ball_body.x_pos;
        ball_body.last_y_pos = ball_body.y_pos;
        ball_body.x_pos = ball_body.x / 256;
        ball_body.y_pos = ball_body.y / 256;
    }

    move_sprite(BALL_SPRITE_ID, ball_body.x_pos-4, ball_body.y_pos-4);
}