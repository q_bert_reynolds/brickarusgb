#include <gb/gb.h>
#include <gb/cgb.h>
#include <stdio.h>

// GameBoy palettes
#define PALETTE_0 0xE0
#define PALETTE_1 0xD0

// GB color palettes
#define RGB_DK_PINK   RGB(31,  5, 10)
#define RGB_HOT_PINK  RGB(31, 10, 19)
#define RGB_MID_PINK  RGB(31, 18, 22)
#define RGB_LT_PINK   RGB(31, 25, 28)
#define RGB_LT_YELLOW RGB(31, 31, 20)
#define RGB_DK_YELLOW RGB(30, 28, 17)
#define RGB_MUTE_RED  RGB(23, 14, 14)
#define RBG_LT_BLUE   RGB(18, 21, 28)
extern UWORD menu_palette[];
extern UWORD game_palette[];
extern UWORD sprite_palette[];

// sprite props
#define USE_PAL 0x10
#define FLIP_X_PAL (S_FLIPX | USE_PAL)
#define FLIP_Y_PAL (S_FLIPY | USE_PAL)
#define FLIP_XY_PAL (FLIP_X_PAL | FLIP_Y_PAL)
#define FLIP_XY (S_FLIPX | S_FLIPY)

// start
#define START_TILE_COUNT 174
extern UBYTE start_screen_tiles[];
extern UBYTE start_screen_map[];
extern UBYTE start_screen_map_palettes[];
void start_screen();

// game
#define BG_TILE_COUNT 88
#define HEART_TILES_INDEX BG_TILE_COUNT
#define NUM_TILES_INDEX (HEART_TILES_INDEX+8)
#define BLANK_TILE_INDEX 0x3F
extern UBYTE ui_tiles[];
extern UBYTE bg_tiles[];
extern UBYTE bg_map[];
extern UBYTE bg_palettes[];
extern UBYTE heart_tiles[];
extern UBYTE no_heart_tiles[];
extern UBYTE num_tiles[];
extern UBYTE frame;
extern UDWORD temp;
extern UDWORD score;
extern UBYTE digits[20];
extern UBYTE i, j;
extern BYTE  x, y;
extern UBYTE lives;
extern UBYTE key;
void update_hearts();
void update_score();
void game_loop();
void cleanup();

// ball
#define BALL_TILES_INDEX 0
#define BALL_SPRITE_ID 0
#define LAUNCH_SPEED 684
#define GRAVITY 8
extern UBYTE ball_sprite_data[];
extern UBYTE ball_on_paddle;
void update_ball();

// paddle
#define HIT_DURATION 16
#define PADDLE_TILES_INDEX BALL_TILES_INDEX+2
#define PADDLE_SPRITE_ID BALL_SPRITE_ID+1
extern UBYTE paddle_sprite_data[];
extern UBYTE paddle_idle_frames[];
extern UBYTE paddle_idle_props[];
extern UBYTE paddle_hit_center_frames[];
extern UBYTE paddle_hit_center_props[];
extern UBYTE paddle_hit_right_frames[];
extern UBYTE paddle_hit_right_props[];
extern UBYTE paddle_hit_left_frames[];
extern UBYTE paddle_hit_left_props[];
extern BYTE paddle_hit_countdown;
extern BYTE paddle_pos;
extern WORD paddle_vel;
void set_paddle_idle();
void set_paddle_hit(BYTE x);
void move_paddle();

// enemies
#define ENEMY_TILES_INDEX PADDLE_TILES_INDEX+16
#define ENEMY_SPRITE_ID PADDLE_SPRITE_ID+4
#define ENEMY_ROWS 3
#define ENEMY_COLS 5
#define ENEMY_COUNT (ENEMY_ROWS * ENEMY_COLS)
extern UBYTE sine_wave[];
extern UBYTE enemy_sprite_data[];

#ifndef ENEMY_STRUCT
#define ENEMY_STRUCT
struct enemy {
    UBYTE x_pos;
    UBYTE y_pos;
    UBYTE alive;
    UBYTE type_index;
    UBYTE offset;
};
#endif

extern struct enemy enemies[ENEMY_COUNT];
void drop_enemies();
void update_enemies();

//sounds
void bounce_sound();
void hit_brick_sound();
void hit_paddle_sound();
void lose_life_sound();