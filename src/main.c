#include "brickarus.h"
#include "palettes.c"
#include "sounds.c"
#include "music.c"
#include "ball.c"
#include "paddle.c"
#include "enemies.c"
#include "start.c"
#include "game.c"

UBYTE frame;
UDWORD temp;
UDWORD score;
UBYTE digits[20];
UBYTE ball_on_paddle = 1u;
UBYTE i, j = 0u;
BYTE x, y = 0;
UBYTE lives = 3u;
UBYTE key = 0u;
BYTE paddle_hit_countdown = 0;
BYTE paddle_pos = 72;
WORD paddle_vel = 0;

void main () {
    NR52_REG = 0xFFU;
    NR51_REG = 0x00U;
    NR50_REG = 0x77U;
    cgb_compatibility();
    DISPLAY_OFF;
    SPRITES_8x16;
    OBP0_REG = PALETTE_0;
    OBP1_REG = PALETTE_1;
    set_sprite_palette(0, 7, sprite_palette);
    SHOW_SPRITES;
    SHOW_BKG;
    while (1) {
        start_screen();
        game_loop();
    }
}
